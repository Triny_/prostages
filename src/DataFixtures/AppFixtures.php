<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Formation;
use App\Entity\Entreprise;
use App\Entity\Stage;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      // Creation du jeu de donnees Faker
        $faker = \Faker\Factory::create('fr_FR');

       // Création des formations

          $formation = new Formation();
          $formation->setType("DUT");
          $formation->setDescription("Informatique");
          $manager->persist($formation);

          $formation = new Formation();
          $formation->setType("Licence professionnelle");
          $formation->setDescription("Métiers du numérique");
          $manager->persist($formation);

          $formation = new Formation();
          $formation->setType("Licence professionnelle");
          $formation->setDescription("Programmation avancée");
          $manager->persist($formation);

      // Création des entreprises
        $nbEntreprises = 7;

        for($i=0; $i<=$nbEntreprises; $i++){

          $entreprise = new Entreprise();
          $entreprise->setNom($faker->company);
          $entreprise->setAdresse($faker->address);
          $entreprise->setTelephone($faker->regexify('(0)5[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'));
          $entreprise->setLienSite($faker->url);
          $manager->persist($entreprise);

        }

      // Création des stages
        $nbStages = 15;

        for($i=0; $i<=$nbEntreprises; $i++){
          $stage = new Stage();
          $stage->setTitre($faker->text($maxNbChars = 20));
          $stage->setDescription($faker->realText($maxNbChars = 200, $indexSize = 2));
          $stage->setDateParution($faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now', $timezone = 'Europe/Paris' ));
          $stage->setContact($faker->regexify('0[6-7][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'));

          // Lier les stages aux formations et aux entreprises
          $stage->addFormation($formation);
          $entreprise->addStage($stage);


          $manager->persist($stage);


        }

        $manager->flush();
    }
}
