<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use App\Entity\Entreprise;
use App\Entity\Stage;
use App\Entity\Formation;

class ProstagesController extends AbstractController
{
    /**
     * @Route("/", name="prostages")
     */
    public function index()
    {
        $listeStages=$this->getDoctrine()->getRepository(Stage::class)->findAll();
        return $this->render('prostages/index.html.twig', [ "Stages" => $listeStages 
        ]);
    }

	/**
     * @Route("/entreprises", name="entreprises")
     */
    public function afficherEntreprises()
    {
        $listeEntreprises=$this->getDoctrine()->getRepository(Entreprise::class)->findAll();
        return $this->render('prostages/affichageEntreprises.html.twig', [
          "Entreprises" => $listeEntreprises
        ]);
    }

	/**
     * @Route("/formations", name="formations")
     */
    public function afficherFormations()
    {
        $listeFormations=$this->getDoctrine()->getRepository(Formation::class)->findAll();
        return $this->render('prostages/affichageFormations.html.twig', [
          "Formations" => $listeFormations
        ]);
    }

	/**
     * @Route("/stages/{id}", name="stages")
     */
    public function afficherStage($id)
    {

        return $this->render('prostages/affichageStage.html.twig', [ 'id' => $id
        ]);
    }
}
